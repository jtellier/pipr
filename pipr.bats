#!/usr/bin/env bats

piprPath=`pwd | xargs -I{} echo "{}/pipr"`
workingDirectory=`pwd | xargs -I{} echo "{}/tmp_bats"`

function setup() {
    export VIRTUAL_ENV=""
    mkdir -p "$workingDirectory"
    pushd $workingDirectory
}

function teardown() {
    popd
    rm -rf "$workingDirectory"    
}

contains() {
    string="$1"
    substring="$2"
    if test "${string#*$substring}" != "$string"
    then
        return 0    # $substring is in $string
    else
        return 1    # $substring is not in $string
    fi
}

@test "given shell is out of virtualenv and no options is used when calling pipr then display out of venv and help" {
  # given
  export VIRTUAL_ENV=""
  
  # when
  result="$($piprPath)"
  
  # then
  contains "$result" "You are out of a virtualenv" \
  && contains "$result" "pipr : a pip wrapper saving top level dependencies to a file"
}

@test "given shell is in a virtualenv and no options is used when calling pipr then display error usage and help" {
  # given
  export VIRTUAL_ENV="$workingDirectory/venv"
  mkdir -p "$workingDirectory/venv"

  # when
  result="$($piprPath 2>&1)"
  
  # then
  contains "$result" "pipr: invalid usage" \
  && contains "$result" "pipr : a pip wrapper saving top level dependencies to a file"
}

@test "given help option is used when calling pipr then display help" {
  # given
  export VIRTUAL_ENV="$workingDirectory/venv"
  mkdir -p "$workingDirectory/venv"
  
  # when
  result="$($piprPath --help)"
  
  # then
  contains "$result" "pipr : a pip wrapper saving top level dependencies to a file"
}

@test "given start option is used when calling pipr then create virtual env and display activation message" {
  # given
  function which () {
    echo "fake/python/path"
  }
  export -f which

  function virtualenv () {
    echo "$@" > /tmp/ni
    if [ "$1" = "-p" ] && [ "$2" = "fake/python/path" ] && [ "$3" = "venv" ]
    then
      return 0 # yes
    else
      return 1 # no
    fi
  }
  export -f virtualenv

  # when
  result="$($piprPath --start)"
  
  # then
  [ "$result" = "Activate the venv with: source venv/bin/activate" ]
}

@test "given prune option is used when calling pipr then uninstall freeze deps and install toplevel ones" {
  # given
  export VIRTUAL_ENV="$workingDirectory/venv"
  mkdir -p "$workingDirectory/venv"

  function pip () {
    echo "$@" >> /tmp/ni

    if [ "$1" = "freeze" ]
    then
      echo -e "testa==1.0\ntestb==2.0"
    elif [ "$1" = "uninstall" ]
    then
      if [ "$3" = "testa" ] || [ "$3" = "testb" ]
      then 
        return 0
      else
        return 1
      fi
    elif [ "$1" = "install" ]
    then
      requirementsFile="toplevelrequirements.txt"
      if test "${3#*$requirementsFile}" != "$3"
      then
        return 0
      else
        return 1
      fi
    fi
  }
  export -f pip
  
  # when
  result="$($piprPath --prune)"
}
