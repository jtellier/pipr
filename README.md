# pipr

Help message :

```bash
    pipr : a pip wrapper saving top level dependencies to a file

    general commands :
            -h, --help : displays this help message
            --autocomplete : install pipr with autocompletion for options and remove command (sudoer)

    virtual env commands :
            -i, --install : install and save given dependencies
            -p, --prune : uninstalled dependencies unrelated to pipr requirements
            -r, --remove <args> : remove given dependencies 

    out of virtualenv commands :
            -s, --start : create a virtual env from python3 with a venv directory
```